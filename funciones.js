var contexto;
 
function inicializar() {
    let lienzo = document.getElementById('miLienzo');
    lienzo.width = window.innerWidth;
    lienzo.height = window.innerHeight;
    contexto = lienzo.getContext('2d');
    contexto.beginPath();
    contexto.lineWidth = 1;
    contexto.strokeStyle = "#CCCCCC";
    contexto.moveTo(0, (window.innerHeight / 2)-100);
    contexto.lineTo(window.innerWidth, (window.innerHeight / 2)-100);
    contexto.stroke();
    contexto.translate(0, window.innerHeight/2);
    contexto.scale(1, -1);
}
 
 
 
 
function velY(){
 
    let vy= document.getElementById('velocidad').value;
    
    let ang= document.getElementById('angulo').value;
    
    let Vinicialy= parseInt(vy)*Math.sin(parseInt(ang)*(Math.PI/180));
 
   
 
 
 
return Vinicialy}
 
function velX(){
 
    let vx= document.getElementById('velocidad').value;
    
    let ang= document.getElementById('angulo').value;
 
    let Vinicialx= parseInt(vx)*Math.cos(parseInt(ang)*(Math.PI/180));// COn transformacion de angulos a radianes
 
 
 
 
return Vinicialx}
 
 
function dibujarcoord(x,y,tpunto){
 
        var tpunto = parseInt(tpunto); 
        
    
        contexto.fillStyle = 'green';     
        contexto.beginPath(); 
        contexto.arc(x, y, tpunto, 0, Math.PI * 2, true); 
        contexto.fill(); 
    
}
 
function Borrar(){
    contexto.clearRect(0,-150,window.innerWidth,window.innerHeight);
}
 
 
function simular(){
    
    let Vinicialx = velX();
    let Vinicialy = velY();
 
      var y=0,x=0,t=0;
 
        while(y>=0){
            y = Vinicialy*t-0.5*9.8*t*t;
            x = Vinicialx*t;
            console.log(+x+ "," +y+ "   en " +t )
            t+=0.002;
            dibujarcoord(x+30,y-100,4);
           //contexto.fillStyle = 'green';
            //drawCoordinates(x+20,y-100,1/2);
 
        }
 
}
